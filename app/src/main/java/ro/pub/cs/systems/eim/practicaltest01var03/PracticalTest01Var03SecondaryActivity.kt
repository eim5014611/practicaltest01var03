package ro.pub.cs.systems.eim.practicaltest01var03

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.practicaltest01var03.ui.theme.PracticalTest01Var03Theme

class PracticalTest01Var03SecondaryActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val result = intent.getStringExtra("result")
        setContent {
            Column{
                Spacer(modifier = Modifier.padding(16.dp))
                Text(text = "$result", modifier = Modifier.padding(8.dp))
                Row{
                    Button(
                        onClick = {
                            setResult(RESULT_OK)
                            finish()
                        },
                        modifier = Modifier
                            .padding(16.dp))
                    {
                        Text("CORRECT")
                    }

                    Spacer(modifier = Modifier.padding(16.dp))
                    Button(
                        onClick = {
                            setResult(RESULT_CANCELED)
                            finish()
                        },
                        modifier = Modifier
                            .padding(16.dp))
                    {
                        Text("INCORRECT")

                    }

                }

            }
        }
    }
}

