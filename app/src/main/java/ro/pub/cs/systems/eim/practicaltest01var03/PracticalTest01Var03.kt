package ro.pub.cs.systems.eim.practicaltest01var03

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.practicaltest01var03.ui.theme.PracticalTest01Var03Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ColocviuUi()
        }
    }
}

@Composable
fun ColocviuUi(){
    val context = LocalContext.current
    val input1 = rememberSaveable { mutableStateOf("")}
    val input2 = rememberSaveable { mutableStateOf("")}
    val result = remember { mutableStateOf("")}
    val textLabel = rememberSaveable { mutableStateOf("")}

    val activityResultsLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult())
    {result ->
        if (result.resultCode == Activity.RESULT_OK) {
            Toast.makeText(context, "The activity returned with result CORRECT", Toast.LENGTH_LONG).show()

        }
        else if (result.resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(context, "The activity returned with result INCORRECT", Toast.LENGTH_LONG).show()

        }
    }



    Column{
        Spacer(modifier = Modifier.padding(8.dp))
        OutlinedTextField(value = input1.value, onValueChange = {input1.value = it}, modifier = Modifier
            .padding(start = 8.dp))
        Spacer(modifier = Modifier.padding(8.dp))
        Row{
            Button(
                onClick = {
                    if(input1.value.toIntOrNull() != null && input2.value.toIntOrNull() != null)
                    {
                        result.value = (input1.value.toInt() + input2.value.toInt()).toString()
                        textLabel.value = "${input1.value} + ${input2.value} = ${result.value}"}
                    else{
                        Toast.makeText(context, "Please enter valid numbers", Toast.LENGTH_SHORT).show()
                    }

                          },
                modifier = Modifier
                    .weight(1f)
                    .padding(16.dp))
            {
                Text("+")
            }

            Spacer(modifier = Modifier.padding(16.dp))
            Button(
                onClick = {
                    if(input1.value.toIntOrNull() != null && input2.value.toIntOrNull() != null)
                    {
                        result.value = (input1.value.toInt() + input2.value.toInt()).toString()
                        textLabel.value = "${input1.value} + ${input2.value} = ${result.value}"}
                    else{
                        Toast.makeText(context, "Please enter valid numbers", Toast.LENGTH_SHORT).show()
                    }
                },
                modifier = Modifier
                    .weight(1f)
                    .padding(16.dp))
            {
                Text("-")

            }
        }
        Spacer(modifier = Modifier.padding(8.dp))
        OutlinedTextField(value = input2.value, onValueChange = {input2.value = it}, modifier = Modifier
            .padding(start = 8.dp))
        Spacer(modifier = Modifier.padding(8.dp))
        Text(text = " ${textLabel.value}", modifier = Modifier.padding(8.dp))
        Spacer(modifier = Modifier.padding(8.dp))
        Button(
            onClick = {
                val intent = Intent(context, PracticalTest01Var03SecondaryActivity::class.java)
                intent.putExtra("result", textLabel.value)
                activityResultsLauncher.launch(intent)
            },
            modifier = Modifier
                .padding(16.dp)
                .align(Alignment.CenterHorizontally))
        {
            Text("Navigate to secondary activity")

        }

    }
}

